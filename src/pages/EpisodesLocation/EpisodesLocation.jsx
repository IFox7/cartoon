import React, { useEffect, useState } from "react";
import { Link, Route, Routes } from "react-router-dom";

import HeroesCards from "../HeroesCards";

import "./episodeslocation.css";

// Функціональний елемент для рендера сторінок Location&&Episode
function EpisodesLocation({ categoryUrl, setArrCharacters, setTitlePage }) {
  // Стейти функції
  const [page, setPage] = useState(categoryUrl);
  const [dataCharacters, setDataCharacters] = useState({ page });
  // Асинхронна функція запиту даних API
  async function fetchData(url) {
    const result = await fetch(url);
    const data = await result.json();
    setDataCharacters(data);
  }
  // Функція пагінації на сторінці
  function pagination() {
    dataCharacters.info.next
      ? setPage(dataCharacters.info.next)
      : setPage(categoryUrl);
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  }
  // Хук для визначення сторінки,що повинна рендеритись(Location vs Episode)
  useEffect(() => {
    setPage(categoryUrl);
  }, [categoryUrl]);
  // Хук для отримання даних з сервера через fetch запит
  useEffect(() => {
    fetchData(page);
  }, [page]);
  // Функція відбору даних для героїв,задіяних в певних серіях або, що знаходяться в певних локаціях
  function heroesEpisode(e) {
    const newArrHeroes = [];
    dataCharacters.results.map((elem) => {
      if (elem.name === e.target.dataset.name) {
        return (
          categoryUrl.split("/")[4] === "episode"
            ? elem.characters
            : elem.residents
        ).map((el) => {
          return newArrHeroes.push(+el.split("/")[5]);
        });
      }
      return elem;
    });
    setArrCharacters(newArrHeroes);
    categoryUrl.split("/")[4] === "episode"
      ? setTitlePage("Актори задіяні в епізоді " + e.target.dataset.name)
      : setTitlePage("Резиденти локації " + e.target.dataset.name);
  }
  // Рендер сторінки Location або Episode ,в залежності від переданих пропсів
  if (Array.isArray(dataCharacters.results)) {
    if (categoryUrl.split("/")[4] === "location") {
      return (
        <>
          <div className="cardsContainer">
            {dataCharacters.results.map((elem) => {
              return (
                <div
                  key={elem.id}
                  className="smallcardWrapper"
                  onClick={heroesEpisode}
                >
                  <div className="dimension">{elem.type}</div>
                  <div className="smallcardTextWrapper">
                    <Link
                      className="smallcardName"
                      to="/heroesCards"
                      data-name={elem.name}
                    >
                      {elem.name}
                    </Link>
                    <div className="cardSpecies">{elem.species}</div>
                  </div>
                </div>
              );
            })}
          </div>
          <div className="btnWrapper">
            <button className="paginateButton" onClick={pagination}>
              load more
            </button>
          </div>

          <Routes>
            <Route path="heroesCards" element={<HeroesCards />}></Route>
          </Routes>
        </>
      );
    } else if (categoryUrl.split("/")[4] === "episode") {
      return (
        <>
          <div className="cardsContainer">
            {dataCharacters.results.map((elem) => {
              return (
                <div
                  key={elem.id}
                  className="smallcardWrapper"
                  onClick={heroesEpisode}
                >
                  <div className="airDate">
                    {" "}
                    Дата першого показу : {elem.air_date}{" "}
                  </div>
                  <div className="smallcardTextWrapper">
                    <Link
                      className="smallcardName"
                      to="/heroesCards"
                      data-name={elem.name}
                    >
                      Серія: {elem.name}
                    </Link>
                  </div>
                </div>
              );
            })}
          </div>
          <div className="btnWrapper">
            <button className="paginateButton" onClick={pagination}>
              load more
            </button>
          </div>
          <Routes>
            <Route path="heroesCards" element={<HeroesCards />}></Route>
          </Routes>
        </>
      );
    }
  }
}

export default EpisodesLocation;
