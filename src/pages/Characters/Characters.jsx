import React, { useEffect, useState } from "react";
import { motion } from "framer-motion";

import "./charcters.css";

const hiddenMask = `repeating-linear-gradient(to right, rgba(0,0,0,0) 0px, rgba(0,0,0,0) 30px, rgba(0,0,0,1) 30px, rgba(0,0,0,1) 30px)`;
const visibleMask = `repeating-linear-gradient(to right, rgba(0,0,0,0) 0px, rgba(0,0,0,0) 0px, rgba(0,0,0,1) 0px, rgba(0,0,0,1) 30px)`;
// Функціональний елемент для відображення всіх головних героїв проекту
function Characters({ categoryUrl }) {
  const [isLoaded, setIsLoaded] = useState(false);
  const [isInView, setIsInView] = useState(false);
  const [page, setPage] = useState(categoryUrl);
  const [dataCharacters, setDataCharacters] = useState({ page });
  async function fetchData(url) {
    const result = await fetch(url);
    const data = await result.json();
    setDataCharacters(data);
  }
  function pagination() {
    if (dataCharacters.info.next) {
      setPage(dataCharacters.info.next);
      window.scrollTo({
        top: 0,
        behavior: "smooth",
      });
    } else return;
  }
  useEffect(() => {
    fetchData(page);
  }, [page]);

  if (Array.isArray(dataCharacters.results)) {
    return (
      <>
        <div className="cardsContainer">
          {dataCharacters.results.map((elem) => {
            return (
              <div key={elem.id} className="cardWrapper">
                <div className="card">
                  <div className="cardBack">
                    <p className="typeHero">Type:{elem.type}</p>
                    <p>Gender:{elem.gender}</p>
                    <p>Status:{elem.status}</p>
                  </div>
                  <div className="cardFront">
                    <motion.div
                      initial={false}
                      animate={
                        isLoaded && isInView
                          ? {
                              WebkitMaskImage: visibleMask,
                              maskImage: visibleMask,
                            }
                          : {
                              WebkitMaskImage: hiddenMask,
                              maskImage: hiddenMask,
                            }
                      }
                      transition={{ duration: 1, delay: 0.4 }}
                      viewport={{ once: true }}
                      onViewportEnter={() => setIsInView(true)}
                      className="cardImg"
                    >
                      <img
                        onLoad={() => setIsLoaded(true)}
                        src={elem.image}
                        alt={elem.name}
                      />
                    </motion.div>
                    <div className="cardTextWrapper">
                      <div className="cardName">{elem.name}</div>
                      <div className="cardSpecies">{elem.species}</div>
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
        <div className="btnWrapper">
          <button className="paginateButton" onClick={pagination}>
            load more
          </button>
        </div>
      </>
    );
  }
}

export default Characters;
