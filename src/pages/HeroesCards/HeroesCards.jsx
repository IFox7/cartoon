import React, { useEffect, useState } from "react";

import { motion } from "framer-motion";
import "./heroesCards.css";
// Змінні для ефекту відкриття зображень бібліотеки motion
const hiddenMask = `repeating-linear-gradient(to right, rgba(0,0,0,0) 0px, rgba(0,0,0,0) 30px, rgba(0,0,0,1) 30px, rgba(0,0,0,1) 30px)`;
const visibleMask = `repeating-linear-gradient(to right, rgba(0,0,0,0) 0px, rgba(0,0,0,0) 0px, rgba(0,0,0,1) 0px, rgba(0,0,0,1) 30px)`;

// Функція для відображення карточок головних героїв серій,або локацій
function HeroesCards({ categoryUrl, arr, titlePage }) {
  const [isLoaded, setIsLoaded] = useState(false);
  const [isInView, setIsInView] = useState(false);
  const [page] = useState(categoryUrl + "/" + arr + ",");
  const [dataCharacters, setDataCharacters] = useState({ page });
  async function fetchData(url) {
    const result = await fetch(url);
    const data = await result.json();
    setDataCharacters(data);
  }

  useEffect(() => {
    fetchData(page);
  }, [page]);

  if (Array.isArray(dataCharacters)) {
    return (
      <>
        <h2>{titlePage}</h2>
        <div className="heroesCardsContainer">
          {dataCharacters.map((elem) => {
            return (
              <div key={elem.id} className="heroesCardWrapper">
                <motion.div
                  initial={false}
                  animate={
                    isLoaded && isInView
                      ? { WebkitMaskImage: visibleMask, maskImage: visibleMask }
                      : { WebkitMaskImage: hiddenMask, maskImage: hiddenMask }
                  }
                  transition={{ duration: 1, delay: 0.5 }}
                  viewport={{ once: true }}
                  onViewportEnter={() => setIsInView(true)}
                  className="cardImg"
                >
                  <img
                    onLoad={() => setIsLoaded(true)}
                    src={elem.image}
                    alt={elem.name}
                  />
                </motion.div>
                <div className="cardTextWrapper">
                  <div className="heroesCardName">{elem.name}</div>
                  <div className="cardSpecies">{elem.species}</div>
                </div>
              </div>
            );
          })}
        </div>
        <div className="btnWrapper"></div>
      </>
    );
  }
}

export default HeroesCards;
