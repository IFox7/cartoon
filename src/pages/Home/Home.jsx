import React from "react";

import "./home.css";

import imageHomePage from "../../elements/images/rick-and-morty2 1.png";

// Головна сторінка проекту
function Home() {
  return (
    <>
      <div className="homePageContainer">
        <div className="imageHomePage">
          <img src={imageHomePage} alt="img" />
        </div>
        <form action="">
          <input
            id="searchImg"
            type="text"
            placeholder={"      Filter by name or episode (ex. S01 or S01E02)"}
          />
        </form>
      </div>
    </>
  );
}

export default Home;
