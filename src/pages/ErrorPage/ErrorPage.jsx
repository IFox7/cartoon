import React from "react";

import "./errorPage.css";
// Функція для відображення помилки при неможливості завантаження сторінки
function ErrorPage() {
  return (
    <>
      <h1>Page not found. Please try again with correct address line.</h1>
    </>
  );
}

export default ErrorPage;
