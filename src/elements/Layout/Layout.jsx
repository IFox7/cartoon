import React, { useState } from "react";

import { Outlet, NavLink } from "react-router-dom";
import logo from "../images/logo-black 1.png";

import "./layout.css";
// Елемент головного меню
export default function Layout({ dataBase }) {
  const [open, setOpen] = useState(false);
  // Відкриття-закриття гамбургер меню
  function hamburgerMenuAnimate() {
    setOpen(!open);
  }
  return (
    <>
      <div className="headerWrapper">
        <NavLink className="logo" to="../../Home">
          <img src={logo} alt="logo" className="logoImg" />
        </NavLink>
        <div className="menuWrapper">
          <ul className={`${open ? "" : "unvisible"}`}>
            {Object.entries(dataBase).map((elem, id) => {
              return (
                <li key={id}>
                  <NavLink className="linkMenu" to={elem[0]}>
                    {elem[0]}
                  </NavLink>
                </li>
              );
            })}
          </ul>
          <div
            onClick={hamburgerMenuAnimate}
            className={`${open ? "hamburger_menu open" : "hamburger_menu"}`}
          >
            <span></span>
          </div>
        </div>
      </div>
      <Outlet />
    </>
  );
}
