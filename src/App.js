import { useEffect, useState } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
// Імпорт елементів проекту
import Layout from "./elements/Layout";
import EpisodesLocation from "./pages/EpisodesLocation";
import Characters from "./pages/Characters";
import Home from "./pages/Home";
import ErrorPage from "./pages/ErrorPage";
import HeroesCards from "./pages/HeroesCards";
// Функція ,що збирає всі елементи проекту і рендерить їх. Передаємо необхідні пропси
function App() {
  const [dataBase, setDataBase] = useState({});
  // Початкове посилання для отримання бази даних
  const [url] = useState("https://rickandmortyapi.com/api");
  async function fetchData(url) {
    const result = await fetch(url);
    const data = await result.json();
    setDataBase(data);
  }
  useEffect(() => {
    const url1 = url;
    fetchData(url1);
  }, [url]);
  // Стейти для відбору масивів головних героїв,заголовків серій,або локацій
  const [arrCharacters, setArrCharacters] = useState([]),
    [titlePage, setTitlePage] = useState("");
  return (
    <Router>
      <Layout dataBase={dataBase} />
      <Routes>
        <Route path="/" element={<Layout />} />
        <Route index element={<Home />} />
        <Route
          path="characters"
          element={<Characters categoryUrl={dataBase.characters} />}
        />
        <Route path="home" element={<Home />} />
        <Route
          path="heroesCards"
          element={
            <HeroesCards
              categoryUrl={dataBase.characters}
              arr={arrCharacters}
              titlePage={titlePage}
            />
          }
        />
        <Route
          path="episodes/*"
          element={
            <EpisodesLocation
              categoryUrl={dataBase.episodes}
              setArrCharacters={setArrCharacters}
              setTitlePage={setTitlePage}
            />
          }
        />
        <Route
          path="locations/*"
          element={
            <EpisodesLocation
              categoryUrl={dataBase.locations}
              setArrCharacters={setArrCharacters}
              setTitlePage={setTitlePage}
            />
          }
        />
        <Route path="*" element={<ErrorPage />} />
      </Routes>
    </Router>
  );
}

export default App;
